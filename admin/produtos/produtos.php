<?php 
$active = 'produtos';
include '../../static/cabecalho.php'; ?>

<div class="container">
		
		<div class="card conteudo">

			<div class="card-header">
				<h4>Produtos Cadastrados</h4>
			</div>

			<div class="card-body">
				<table class="table table-striped">
				  <thead>
				    <tr>
				      <th scope="col">Código</th>
				      <th scope="col">Nome</th>
				      <th scope="col">Categoria</th>
				      <th scope="col">Unidade</th>
				      <th scope="col">Preço Unitário</th>
				      <th scope="col">Estoque</th>
				      <th scope="col">Descrição</th>
				      <th scope="col">Ações</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <th scope="row">1</th>
				      <td>Maçã</td>
				      <td>Alimentos</td>
				      <td>Caixa</td>
				      <td>2,00</td>
				      <td>251</td>
				      <td></td>
				      <td>
				      	<a class="btn btn-primary"><i style="color: white;" class="fa fa-edit"></i></a>
						<a class="btn btn-danger"><i style="color: white;" class="fa fa-trash-alt"></i></a>
				      </td>
				    </tr>
				  </tbody>
				</table>
			</div>

		</div>
</div>


<?php include '../../static/rodape.php'; ?>