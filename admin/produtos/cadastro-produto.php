<?php 
$active = 'produtos';
include '../../static/cabecalho.php'; ?>

<div class="container">
		
		<div class="card conteudo">

			<div class="card-header">
				<h4>Cadastro de Produto</h4>
			</div>

			<div class="card-body">

				<form>
					<fieldset>

					<div class="form-group row">
					    <label class="col-md-4 control-label" for="txtproduto">Imagem : </label>  
					    <div class="col-md-6">
						  <div class="custom-file">
						    <input type="file" accept="image/gif, image/jpeg, image/png" 
						    class="custom-file-input" id="inputFile">
						    <label class="custom-file-label" id="inputFileName" for="inputFile">Selecionar Imagen</label>
						  </div>
					 	</div>
				    </div>

					<!-- Text input-->
					<div class="form-group row">
					  <label class="col-md-4 control-label" for="txtproduto">Produto : </label>  
					  <div class="col-md-6">
					  <input id="txtproduto" name="txtproduto" type="text" placeholder="Nome do produto" class="form-control input-md" required="">
					    
					  </div>
					</div>

					<!-- Select Basic -->
					<div class="form-group row">
					  <label class="col-md-4 control-label" for="txtgrupo">Categoria : </label>
					  <div class="col-md-4">
					    <select id="txtgrupo" name="txtgrupo" class="form-control">
					      <option value="1">Higiêne</option>
					      <option value="2">Consumo</option>
					      <option value="3">Limpesa</option>
					      <option value="4">Material de Escritório</option>
					    </select>
					  </div>
					</div>

					<!-- Select Basic -->
					<div class="form-group row">
					  <label class="col-md-4 control-label" for="txtcodigo_unidade_id">Unidade : </label>
					  <div class="col-md-4">
					    <select id="txtcodigo_unidade_id" name="txtcodigo_unidade_id" class="form-control">
					      <option value="UND">Unidade</option>
					      <option value="CX">Caixa</option>
					      <option value="PC">Pacote</option>
					      <option value="FAR">Fardo</option>
					    </select>
					  </div>
					</div>

					<!-- Text input-->
					<div class="form-group row">
					  <label class="col-md-4 control-label" for="txtestoque_atual">Valor Unitário : </label>  
					  <div class="col-md-2">
					  <input id="txtestoque_atual"
					   step=".01"
					   min="0" value="0"
					   pattern="^\d+(?:\.\d{1,2})?$"
					   placeholder="0.00"
					   required 
					   name="txtestoque_atual" type="text" placeholder="" class="form-control input-md">
					    
					  </div>
					</div>

					<!-- Text input-->
					<div class="form-group row">
					  <label class="col-md-4 control-label" for="txtestoque_atual">Estoque Atual : </label>  
					  <div class="col-md-2">
					  <input id="estoque_atual" name="estoque_atual" 
					  min="0" 
					  type="number" placeholder="" class="form-control input-md">
					    
					  </div>
					</div>

					<!-- Text input-->
					<div class="form-group row">
					  <label class="col-md-4 control-label" for="txtestoque_atual">Descrição : </label>  
					  <div class="col-md-6">
					 	 <textarea class="form-control" id="descricao" name="descricao"  rows="3"></textarea>
					  </div>
					</div>

					<!-- Button (Double) -->
					<div class="form-group">
					  <label class="col-md-4 control-label" for="btnsalvar"></label>
					  <div class="col-md-8">
					    <button id="btnsalvar" name="btnsalvar" class="btn btn-primary">Salvar</button>
					    <button id="btncancelar" name="btncancelar" class="btn btn-danger">Cancelar</button>
					  </div>
					</div>

					</fieldset>
					</form>
				</div>
			</div>
	</div>


<?php include '../../static/rodape.php'; ?>