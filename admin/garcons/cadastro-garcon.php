<?php 
$active = 'garcons';
include '../../static/cabecalho.php'; ?>

<div class="container">
		
		<div class="card conteudo">

			<div class="card-header">
				<h4>Cadastro Garçon</h4>
			</div>

			<div class="card-body">

				<form>
					<fieldset>

					<!-- Text input-->
					<div class="form-group row">
					  <label class="col-md-4 control-label" for="txtnome">Nome : </label>  
					  <div class="col-md-6">
					  <input id="txtnome" name="txtnome" type="text" placeholder="" class="form-control input-md" required>
					    
					  </div>
					</div>

					<!-- Text input-->
					<div class="form-group row">
					  <label class="col-md-4 control-label" for="txtemail">E-Mail : </label>  
					  <div class="col-md-6">
					  <input id="txtemail" name="txtemail" 
                            type="email" placeholder="" 
                            class="form-control input-md" required>
					    
					  </div>
					</div>

					<!-- Text input-->
					<div class="form-group row">
					  <label class="col-md-4 control-label" for="txtsenha">Senha : </label>  
					  <div class="col-md-6">
					  <input id="txtsenha" name="txtsenha" 
                        type="text" placeholder="******" 
                        class="form-control input-md" required>
					    
					  </div>
					</div>

					<!-- Button (Double) -->
					<div class="form-group">
					  <label class="col-md-4 control-label" for="btnsalvar"></label>
					  <div class="col-md-8">
					    <button id="btnsalvar" name="btnsalvar" class="btn btn-primary">Salvar</button>
					    <button id="btncancelar" name="btncancelar" class="btn btn-danger">Cancelar</button>
					  </div>
					</div>

					</fieldset>
					</form>
				</div>
			</div>
	</div>


<?php include '../../static/rodape.php'; ?>