<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="utf-8">
	<title>Feeder</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../../static/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../../static/css/style.css">
	<link rel="stylesheet" href="../../static/font-awesome/all.css">
	<script src="../../static/jquery/jquery-3.3.1.min.js"></script>
	<script src="../../static/popper/popper.min.js"></script>
	<script src="../../static/bootstrap/bootstrap.min.js"></script>
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	  <a class="navbar-brand" href="#">Feeder</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item <?php echo ($active == 'inicio') ? 'active' : '' ?>">
	        <a class="nav-link" href="#">Inicio</a>
	      </li>

	       <li class="nav-item">
		      <a class="nav-link" href="#">Vendas</a>
		   </li>

	      <li class="nav-item dropdown <?php echo ($active == 'produtos') ? 'active' : '' ?>">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Produtos </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="../produtos/produtos.php">Todos os produtos</a>
	          <a class="dropdown-item" href="../produtos/cadastro-produto.php">Cadastrar novo</a>
	        </div>
	      </li>


	      <li class="nav-item dropdown <?php echo ($active == 'clientes') ? 'active' : '' ?>">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Clientes </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="#">Gerenciar Clientes</a>
	          <a class="dropdown-item" href="#">Novo Cliente</a>
	        </div>
	      </li>

	      <li class="nav-item dropdown <?php echo ($active == 'fornecedores') ? 'active' : '' ?>">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Fornecedores </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="#">Gerenciar Fornecedores</a>
	          <a class="dropdown-item" href="../fornecedores/cadastro-fornecedor.php">Cadastrar Fornecedor</a>
	        </div>
	      </li>


	      <li class="nav-item dropdown <?php echo ($active == 'garcons') ? 'active' : '' ?>">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Garçons </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="#">Gerenciar Garçons</a>
	          <a class="dropdown-item" href="../garcons/cadastro-garcon.php">Cadastrar Garçon</a>
	        </div>
	      </li>

	      <li class="nav-item <?php echo ($active == 'relatorios') ? 'active' : '' ?>">
		      <a class="nav-link" href="#">Relatórios</a>
		  </li>

	      <li class="nav-item dropdown <?php echo ($active == 'usuarios') ? 'active' : '' ?>">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Usuários </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="#">
	          	Gerenciar Usuários
	          </a>
	          <a class="dropdown-item" href="#"> Cadastrar Novo  </a>
	        </div>
	      </li>
		</ul>

		<ul class="nav navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="#">Sair</a>
            </li>
        </ul>
	  </div>
	</nav>
	
